import React, { Component } from 'react';

class App extends Component {
  state = {
    person: {
      fullName: 'John Doe',
      bio: 'Je suis un développeur web.',
      imgSrc: 'https://www.shutterstock.com/image-vector/young-man-keeping-hands-on-260nw-1175222617.jpg',
      profession: 'Développeur'
    },
    shows: false,
    mountTime: new Date()
  };

  toggleShow = () => {
    this.setState(prevState => ({
      shows: !prevState.shows
    }));
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({ mountTime: new Date() });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { person, shows, mountTime } = this.state;

    return (
      <div>
        <button onClick={this.toggleShow}>Basculer le profil</button>
        {shows && (
          <div>
            <h2>{person.fullName}</h2>
            <p>{person.bio}</p>
            <img src={person.imgSrc} alt={person.fullName} />
            <p>Profession : {person.profession}</p>
          </div>
        )}
        <p>Monté depuis : {Math.floor((new Date() - mountTime) / 1000)} secondes.</p>
      </div>
    );
  }
}

export default App;
